<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

/**
 * Библиотека Печёного Станислава
 * @var  $pensionAge
 */

$pensionAge = 65;

$employees = [];

$employees[] = new classPension\pensionFond();
$employees[0]->name = 'Vasya';
$employees[0]->age = 36;
$employees[0]->city = 'Kyiv';

//Добавление функции is_countable()
if (is_countable($employees)) {
    // Добавление функций array_key_first() и array_key_last()
    for ($i = array_key_first($employees); $i <= array_key_last($employees); $i++) {
        echo "<b>" . $employees[$i]->name . " :</b><br>";
        echo "Живет в : ";
        $employees[$i]->showCity();
        echo "Осталось до пенсии : " . $employees[$i]->yearsToPension($pensionAge) . ' лет <br><br>';
    }
} else {
    echo 'Error while filling the array!';
}

/**
 * Библиотека Мыгыт Сергея
 */

$calc1 = new devswb\hometask2\discriminantCalc\DiscriminantCalc(2,5,1);
$calc1->Result();

/**
 * Библотека  Педченко Сергея
 */

$square1 = new winterpsv\winter\Calc();
$square1->counts('Rectangle', 3, 8);
echo '<br>';
$square1->getResult();

/**
 * Библиотека  Нечипоренко Сергея
 */

$app = new Nechiporenko\Classes\App();

$app->execute(12, 3);
$app->execute(5, 2);
$app->execute(3, null);
$app->showTotal();

/**
 * Библиотека Поточилова Тараса
 */

$fiche = new \classes\ShowFiche('is_countable');
echo $fiche->showFiche();

/**
 * Библиотека Билана Евгения
 */



/**
 * Библиотека Уманец Александра
 */



/**
 * Библиотека Лысенко Липы
 */

use lysenkolipa\calcLiba\basicCalculator\BasicCalculator;

$calcBase = new BasicCalculator(5.3,4.0);
$calcBase->getMultiplication();
$calcBase->getSubtraction();
$calcBase->getSumm();
$calcBase->displayResult();

/**
 * Библиотека Ткачук Владислава
 */



/**
 * Библиотека Сидор Петра
 */

use psydo\packagisttest\dumbclass\Test1;

$test1 = new Test1();
echo $test1 . '<br>';

/**
 *  Библиотека Пелюховского Сергея
 */

use pelukho\userfaker\src\UserFaker;

$fakeUser = new UserFaker();
$fakeUser = new UserFaker("f");
echo $fakeUser->getUserName();

/**
 *  Библиотека Боровик Виталия
 */



/**
 * Библиотека Тычина Владимира
 */

use Platon\Platon;

$pay = new Platon('TEST000001', 'P@$$vv0r|)');

echo $pay->getHtmlForm([
                           'payment'     => 'CC',
                           'amount'      => '1.99',
                           'currency'    => 'UAH',
                           'description' => 'Test description',
                           'url'         => 'http://exmple.com',
                       ]);
