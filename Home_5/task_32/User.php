<?php

declare(strict_types=1);

namespace task_32;

class User
{
    use Helper;

    /**
     * User constructor.
     * @param $name
     * @param $age
     */
    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }
}
