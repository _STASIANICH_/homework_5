<?php

declare(strict_types=1);

namespace task_32;

class Country
{
    private int $population;
    use Helper;

    /**
     * Country constructor.
     * @param $name
     * @param $age
     * @param $population
     */
    public function __construct($name, $age, $population)
    {
        $this->name = $name;
        $this->age = $age;
        $this->population = $population;
    }

    /**
     * @return int
     */
    public function getPopulation(): int
    {
        return $this->population;
    }
}
