<?php

declare(strict_types=1);

namespace task_17;

class City
{
    public string $name;

    public float $population;

    /**
     * City constructor.
     * @param $name
     * @param $population
     */
    public function __construct($name, $population)
    {
        $this->name = $name;
        $this->population = $population;
    }
}
