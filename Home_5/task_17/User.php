<?php

declare(strict_types=1);

namespace task_17;

class User
{
    public string $name;

    public string $surname;

    /**
     * User constructor.
     * @param $name
     * @param $surname
     */
    public function __construct($name, $surname)
    {
        $this->name = $name;
        $this->surname = $surname;
    }
}
