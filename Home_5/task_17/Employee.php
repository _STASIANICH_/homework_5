<?php

declare(strict_types=1);

namespace task_17;

class Employee extends User
{
    public float $salary;

    /**
     * Employee constructor.
     * @param $name
     * @param $surname
     * @param $salary
     */
    public function __construct($name, $surname, $salary)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->salary = $salary;
    }
}
