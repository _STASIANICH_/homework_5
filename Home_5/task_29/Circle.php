<?php

namespace task_29;

interface Circle
{
    /**
     * @return mixed
     */
    public function getRadius(); // получить радиус  фигуры

    /**
     * @return mixed
     */
    public function getDiameter(); // получить диаметр фигуры
}
