<?php

namespace task_29;

interface Tetragon
{
    /**
     * @return mixed
     */
    public function getSideA(); // получить сторону A  фигуры

    /**
     * @return mixed
     */
    public function getSideB(); // получить сторону B фигуры

    /**
     * @return mixed
     */
    public function getSideC(); // получить сторону C фигуры

    /**
     * @return mixed
     */
    public function getSideD(); // получить сторону D фигуры
}
