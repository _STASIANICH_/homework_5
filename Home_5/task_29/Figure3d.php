<?php

namespace task_29;

interface Figure3d
{
    /**
     * @return mixed
     */
    public function getVolume(); // получить объем фигуры

    /**
     * @return mixed
     */
    public function getSurfaceSquare(); // получить площадь поверхности фигуры
}
