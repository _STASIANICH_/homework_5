<?php

declare(strict_types=1);

namespace task_29;

class Disk implements Figure, Circle
{
    private const Pi = 3.14;

    public float $Radius;

    public function __construct($r)
    {
        $this->Radius = $r;
    }

    /**
     * @return float
     */
    public function getSquare(): float
    {
        return (pow($this->Radius, 2) * self::Pi);
    }

    /**
     * @return float
     */
    public function getPerimeter(): float
    {
        return (2 * $this->Radius * self::Pi);
    }

    /**
     * @return float
     */
    public function getRadius(): float
    {
        return $this->Radius;
    }

    /**
     * @return float
     */
    public function getDiameter(): float
    {
        return ($this->Radius * 2);
    }
}
