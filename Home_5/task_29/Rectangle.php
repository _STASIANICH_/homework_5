<?php

declare(strict_types=1);

namespace task_29;

class Rectangle implements Figure, Tetragon
{
    public float $sideA;
    public float $sideB;
    public float $sideC;

    public function __construct($a, $b, $c)
    {
        $this->sideA = $a;
        $this->sideB = $b;
        $this->sideC = $c;
    }

    /**
     * @return float
     */
    public function getSquare(): float
    {
        $p = (($this->sideA + $this->sideB + $this->sideC) / 2);
        $S = sqrt($p * ($p - $this->sideA) * ($p - $this->sideB) * ($p - $this->sideC));
        return $S;
    }

    /**
     * @return float
     */
    public function getPerimeter(): float
    {
        return $this->sideA + $this->sideB + $this->sideC;
    }

    /**
     * @return float
     */
    public function getSideA(): float
    {
        return $this->sideA;
    }

    /**
     * @return float
     */
    public function getSideB(): float
    {
        return $this->sideB;
    }

    /**
     * @return float
     */
    public function getSideC(): float
    {
        return $this->sideC;
    }

    public function getSideD(): void
    {
        // TODO: Implement getSideD() method.
    }
}
