<?php

declare(strict_types=1);

namespace task_18;

class Post
{
    private string $name;

    private float $salary;

    /**
     * Post constructor.
     * @param string $name
     * @param float $salary
     */
    public function __construct(string $name, float $salary)
    {
        $this->name = $name;
        $this->salary = $salary;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getSalary(): float
    {
        return $this->salary;
    }
}
