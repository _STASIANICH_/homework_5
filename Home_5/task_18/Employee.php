<?php

declare(strict_types=1);

namespace task_18;

class Employee
{
    private string $name;

    private string $surname;

    public $post;

    /**
     * Employee constructor.
     * @param string $name
     * @param string $surname
     */
    public function __construct($name = 'Vasya', $surname = "Pupkin", Post $post)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->post = $post;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param $post
     */
    public function changePost(Post $post): void
    {
        $this->post = $post;
    }
}
