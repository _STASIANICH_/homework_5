<?php

declare(strict_types=1);

namespace task_30;

class Rectangle extends Figure
{
    private float $sideA;
    private float $sideB;
    private float $sideC;

    /**
     * Rectangle constructor.
     * @param $a
     * @param $b
     * @param $c
     */
    public function __construct($a, $b, $c)
    {
        $this->sideA = $a;
        $this->sideB = $b;
        $this->sideC = $c;
    }

    /**
     * @return float
     */
    public function getArea(): float
    {
        $p = (($this->sideA + $this->sideB + $this->sideC) / 2);
        $S = sqrt($p * ($p - $this->sideA) * ($p - $this->sideB) * ($p - $this->sideC));
        return $S;
    }

    /**
     * @return float
     */
    public function getPerimeter(): float
    {
        return $this->sideA + $this->sideB + $this->sideC;
    }
}
