<?php

declare(strict_types=1);

namespace task_30;

abstract class Figure
{
    /**
     * @return float
     */
    abstract public function getArea(): float;

    /**
     * @return float
     */
    abstract public function getPerimeter(): float;

    /**
     * @return float
     */
    public function getSum(): float
    {
        return ($this->getArea() + $this->getPerimeter());
    }
}
