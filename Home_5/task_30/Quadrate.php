<?php

declare(strict_types=1);

namespace task_30;

class Quadrate extends Figure
{
    private float $side;

    /**
     * Quadrate constructor.
     * @param $side
     */
    public function __construct($side)
    {
        $this->side = $side;
    }

    /**
     * @return float
     */
    public function getArea(): float
    {
        return (pow($this->side, 2));
    }

    /**
     * @return float
     */
    public function getPerimeter(): float
    {
        return (4 * $this->side);
    }
}
