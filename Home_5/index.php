<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

/**Tasks 17.1 - 17.7*/

$arr = [];
$arr[] = new task_17\User('Kolia', 'Pupkin');
$arr[] = new task_17\Employee('Stas', 'Pechenyi', 200000);
$arr[] = new task_17\City('Kyiv', 3500000);

$arr[] = new task_17\User('Misha', 'Michael');
$arr[] = new task_17\Employee('Igor', 'Bailer', 3000);
$arr[] = new task_17\City('Cherkasy', 250000);

$arr[] = new task_17\User('Vasya', 'Vasiliev');
$arr[] = new task_17\Employee('Vania', 'Artemov', 1500);
$arr[] = new task_17\City('Odesa', 1700000);

/**Переберите циклом массив $arr и выведите на экран столбец
 * свойств name тех объектов, которые принадлежат классу User
 * или потомку этого класса.
 */

foreach ($arr as $person) {
    if ($person instanceof task_17\User) {
        echo $person->name . '<br>';
    }
}

echo '-------------------<br>';

/**Переберите циклом массив $arr и выведите на экран столбец
 * свойств name тех объектов, которые НЕ принадлежат классу User
 * или потомку этого класса.
 */

foreach ($arr as $person) {
    if (!($person instanceof task_17\User)){
        echo $person->name . '<br>';
    }
}

echo '-------------------<br>';

/**Переберите циклом массив $arr и выведите на экран столбец
 * свойств name тех объектов, которые принадлежат именно классу User,
 * то есть не классу City и не классу Employee
 */

foreach ($arr as $person) {
    if (!($person instanceof task_17\City)
        && !($person instanceof task_17\Employee)) {
        echo $person->name . '<br>';
    }
}

echo '-------------------<br>';

/**Tasks 18.1 - 18.9*/

$programmer = new task_18\Post('programmer', 20000);
$manager = new task_18\Post('manager', 10000);
$driver = new task_18\Post('driver', 18000);

$employee1 = new task_18\Employee('Vasya', 'Pupkin', $programmer);

echo $employee1->getName() . '<br>';
echo $employee1->getSurname() . '<br>';
echo $employee1->post->getName() . '<br>';
echo $employee1->post->getSalary() . '<br>';

/**Реализуйте в классе Employee метод changePost,
 * который будет изменять должность работника на другую
 */

$employee1->changePost($driver);

echo $employee1->post->getName() . '<br>';
echo $employee1->post->getSalary() . '<br>';

/**Tasks 19.1 - 19.6 доделать!*/

$user1 = new task_19\User();
$user1->name = 'Kolia';
$user1->age = 25;
echo $user1->age;
echo '<br>';
$user1->setAgeV2(17);
echo $user1->age;
echo '<br>';

/**Tasks 20.1 - 20.4 переделать!*/

$teacher1 = new task_20\Teacher();
$teacher1->transferToNextCourse();
$teacher1->setCourseAdministrator('Gadia', 'Hrenova','Petrovich');
$teacher1->getCourseAdministrator() . '<br>';

/**Tasks 21.1 - 21.5*/

$employee2 = new task_21\Employee('','','1998-03-23',30000);
echo $employee2->getAge() . '<br>';

/**Tasks 22.1 - 22.4*/

$arr = [1,2,3];

$arr = task_22\ArraySumHelper::getPow($arr, 2);
$sum = task_22\ArraySumHelper::getSum($arr);

echo $sum . '<br>';

/**Tasks 23.1 - 23.4*/

/**
 * 23.1 Выведите сумму значений свойств на экран.
 */

echo task_23\Num::$num1 + task_23\Num::$num2 . '<br>';

/**
 * 23.3 Добавьте в класс Num метод getSum,
 * который будет выводить на экран сумму значений свойств num1 и num3.
 */

$sum = new task_23\Num;
echo $sum->getSum() . '<br>';

/**
 * 23.4 Выведите на экран значение первой константы напрямую,
 * и значение второй константы через геттер.
 */

echo task_23\Num::FIRSTCONST . '<br>';
echo $sum->getSecondConst() . '<br>';

/** Tasks 24.1 - 24.2*/

$user2 = new task_24\User();
$user2->setName('Dima');
$user2->setAge(18);

echo $user2->getName() . '<br>';
echo $user2->getAge() . '<br>';

/** Tasks 25.1 - 25.3*/

$cube1 = new task_25\Cube(4);
echo 'Объем куба: ' . $cube1->getVolume() . '<br>';
echo 'Площадь поверхности куба: ' . $cube1->getArea() . '<br>';

/** Tasks 26.1 - 26.4*/

$user3 = new task_26\User('Vasya', 25);
echo $user3->getName() . ' is ' . $user3->getAge() . ' years old :)<br>';

/** Tasks 27.1 - 27.3*/

$employee3 = new task_27\Employee();
$employee3->setName('Petya');
$employee3->setAge(40);
$employee3->setSalary(50000);

echo $employee3->getName() . ' is ' . $employee3->getAge()
    . ' years old and gains ' . $employee3->getSalary() . ' grn!<br>';

/** Tasks 28.1 - 28.6*/

$arr = [];

$arr[] = new task_28\Cube(2);
$arr[] = new task_28\Quadrate(2);
$arr[] = new task_28\Rectangle(2,3,4);

$arr[] = new task_28\Cube(4);
$arr[] = new task_28\Quadrate(4);
$arr[] = new task_28\Rectangle(5,6,7);

/** Переберите циклом массив $arr и выведите на экран
 * только площади объектов реализующих интерфейс Figure
 */

foreach ($arr as $figure){
    if($figure instanceof task_28\Figure){
        echo $figure->getSquare() . '<br>';
    }
}

/** Переберите циклом массив $arr и выведите
 * для плоских фигур их площади, а для объемных - площади их поверхности.
 */

foreach ($arr as $figure){
    if($figure instanceof task_28\Figure){
        echo 'Площадь плоских фигур: ' . $figure->getSquare() . '<br>';
    }

    if($figure instanceof task_28\Figure3d){
        echo 'Площадь поверхности объемных фигур: '
            . $figure->getSurfaceSquare() . '<br>';
    }
}

/** Tasks 29.1 - 29.3*/

$rectangle1 = new task_29\Rectangle(2,3,4);
echo 'Сторона A: ' . $rectangle1->getSideA() . '<br>';
echo 'Сторона B: ' . $rectangle1->getSideB() . '<br>';
echo 'Сторона C: ' . $rectangle1->getSideC() . '<br>';

$disk1 = new task_29\Disk(3);
echo 'Радиус круга: ' . $disk1->getRadius() . '<br>';
echo 'Диаметр круга: ' . $disk1->getDiameter() . '<br>';
echo 'Периметр круга: ' . $disk1->getPerimeter() . '<br>';
echo 'Площадь круга: ' . $disk1->getSquare() . '<br>';

/** Tasks 30.1 - 30.2 сделать!*/

$quadrate2 = new task_30\Quadrate(3);
$rectangle2 = new task_30\Rectangle(3,4,5);

echo '<b>Квадрат:</b><br>';
echo $quadrate2->getArea() . ' | ' . $quadrate2->getPerimeter() . '<br>';
echo 'Сумма: ' . $quadrate2->getSum() . '<br>';

echo '<b>Триугольник:</b><br>';
echo $rectangle2->getArea() . ' | ' . $rectangle2->getPerimeter() . '<br>';
echo 'Сумма: ' . $rectangle2->getSum() . '<br>';

/** Tasks 31.1 - 31.4 сделать!*/

$quadrate4 = new task_30\Quadrate(3);
$rectangle4 = new task_30\Rectangle(3,4,5);

$collection = new task_31\FiguresCollection();

$collection->addFigure($quadrate4)
           ->addFigure($rectangle4);

echo 'Сумарная площадь фигур: ' . $collection->getTotalSquare() . '<br>';

/** Tasks 32.1 - 32.4 сделать!*/

$user4 = new task_32\User('Vasya', 34);
$country1 = new task_32\Country('Ukraine', 28, 35000000);

echo $user4->getName() . '<br>';
echo $country1->getName() . '<br>';

/** Tasks 33.1 - 33.2*/

echo 'Сумма результатов методов из 3 трейтов: ' . task_33\Test::getSum() . '<br>';

/** Tasks 40.1 - 40. сделать!*/

$privateHouse = new task_40\PrivateHouse(120,
                                         6,
                                         4,
                                         15,
                                         true,
                                         true,
                                         4,
                                         7,
                                         16);
$privateHouse->openOrCloseDoors();
$privateHouse->openOrCloseWindows();
$privateHouse->onOrOffLights();
$privateHouse->openOrCloseDoors();
$privateHouse->openOrCloseWindows();
$privateHouse->onOrOffLights();

