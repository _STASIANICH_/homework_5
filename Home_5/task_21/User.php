<?php

declare(strict_types=1);

namespace task_21;

class User
{
    private string $name;

    private string $surname;

    private string $birthday;

    private $age;

    public function __construct($name = 'Roman', $surname = 'Romanov', $birthday)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->birthday = $birthday;
        $this->age = $this->calculateAge($birthday);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getBirthday(): string
    {
        return $this->birthday;
    }

    /**
     * @return float
     */
    public function getAge(): float
    {
        return $this->age;
    }

    /**
     * @param $birthday
     * @return false|int|mixed|string
     */
    private function calculateAge($birthday)
    {
        $arr = explode('-', $birthday);
        $year = $arr[0];
        $month = $arr[1];
        $day = $arr[2];
        if ($month > date('m') || $month == date('m') && $day > date('d')) {
            return (date('Y') - $year - 1);
        } else {
            return (date('Y') - $year);
        }
    }
}
