<?php

declare(strict_types=1);

namespace task_23;

class Num
{
    const FIRSTCONST = 21;
    const SECONDCONST = 99;

    public static float $num1 = 2;
    public static float $num2 = 3;
    public static float $num3 = 3;
    public static float $num4 = 5;

    /**
     * @return float
     */
    public function getSum(): float
    {
        return self::$num1 + self::$num3;
    }

    /**
     * @return float
     */
    public function getSecondConst(): float
    {
        return self::SECONDCONST;
    }
}


