<?php

namespace task_25;

interface CubeInterface
{
    /**
     * CubeInterface constructor.
     * @param $side
     */
    public function __construct($side); // конструктор, параметром принимающий сторону куба

    /**
     * @return float
     */
    public function getVolume(); // метод для получения объема куба

    /**
     * @return float
     */
    public function getArea(); // метод для получения площади поверхности
}
