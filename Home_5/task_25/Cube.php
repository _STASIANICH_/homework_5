<?php

declare(strict_types=1);

namespace task_25;

class Cube implements CubeInterface
{
    private float $side;

    /**
     * Cube constructor.
     * @param $side
     */
    public function __construct($side)
    {
        $this->side = $side;
    }

    /**
     * @return float
     */
    public function getVolume(): float
    {
        return (pow($this->side, 3));
    }

    /**
     * @return float
     */
    public function getArea(): float
    {
        return (6 * pow($this->side, 2));
    }
}
