<?php

declare(strict_types=1);

namespace task_40;

class Building
{
    private float $area;
    private float $height;
    private int $externalWallsCount;
    private int $internalWallsCount;
    private bool $electricity;
    private bool $gas;
    private int $doorCount;
    private string $doorStatus = 'Close';
    private int $windowCount;
    private string $windowStatus = 'Close';
    private int $lightsCount;
    private string $lightsStatus = 'Off';

    public function __construct(
        $area = 0,
        $height = 0,
        $externalWallsCount = 0,
        $internalWallsCount = 0,
        $electricity = false,
        $gas = false,
        $doorCount = 0,
        $windowCount = 0,
        $lightsCount = 0
    ) {
        $this->area = $area;
        $this->height = $height;
        $this->externalWallsCount = $externalWallsCount;
        $this->internalWallsCount = $internalWallsCount;
        $this->electricity = $electricity;
        $this->gas = $gas;
        $this->doorCount = $doorCount;
        $this->windowCount = $windowCount;
        $this->lightsCount = $lightsCount;
    }

    /**
     *Changes doors status
     */
    public function openOrCloseDoors(): void
    {
        if ($this->doorCount > 0 && $this->doorStatus == 'Close') {
            $this->doorStatus = 'Open';
            echo 'All doors are open<br>';
        } elseif ($this->doorStatus == 'Open') {
            $this->doorStatus = 'Close';
            echo 'All doors are closed<br>';
        }
    }

    /**
     *Changes windows status
     */
    public function openOrCloseWindows(): void
    {
        if ($this->windowCount > 0 && $this->windowStatus == 'Close') {
            $this->windowStatus = 'Open';
            echo 'All windows are open<br>';
        } elseif ($this->windowStatus == 'Open') {
            $this->windowStatus = 'Close';
            echo 'All windows are closed<br>';
        }
    }

    /**
     *Changes lights status
     */
    public function onOrOffLights(): void
    {
        if ($this->lightsCount > 0 && $this->lightsStatus == 'Off') {
            $this->lightsStatus = 'On';
            echo 'All lights are on<br>';
        } elseif ($this->lightsStatus == 'On') {
            $this->lightsStatus = 'Off';
            echo 'All lights are off<br>';
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->$name;
    }
}
