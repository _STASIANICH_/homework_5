<?php

namespace task_26;

interface UserInterface
{
    /**
     * UserInterface constructor.
     * @param $name
     * @param $age
     */
    public function __construct($name, $age); // у юзера будет имя и возраст

    /**
     * @return mixed
     */
    public function getName(); // метод для получения имени юзера

    /**
     * @return mixed
     */
    public function getAge(); // метод для получения возроста юзера
}
