<?php

declare(strict_types=1);

namespace task_28;

class Quadrate implements Figure
{
    public float $side;

    public function __construct($side)
    {
        $this->side = $side;
    }

    /**
     * @return float
     */
    public function getSquare(): float
    {
        return (pow($this->side, 2));
    }

    /**
     * @return float
     */
    public function getPerimeter(): float
    {
        return (4 * $this->side);
    }
}
