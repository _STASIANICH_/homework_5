<?php

declare(strict_types=1);

namespace task_28;

class Rectangle implements Figure
{
    public float $sideA;
    public float $sideB;
    public float $sideC;

    public function __construct($a, $b, $c)
    {
        $this->sideA = $a;
        $this->sideB = $b;
        $this->sideC = $c;
    }

    /**
     * @return float
     */
    public function getSquare(): float
    {
        $p = (($this->sideA + $this->sideB + $this->sideC) / 2);
        $S = sqrt($p * ($p - $this->sideA) * ($p - $this->sideB) * ($p - $this->sideC));
        return $S;
    }

    /**
     * @return float
     */
    public function getPerimeter(): float
    {
        return $this->sideA + $this->sideB + $this->sideC;
    }
}
