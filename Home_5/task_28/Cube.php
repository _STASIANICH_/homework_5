<?php

declare(strict_types=1);

namespace task_28;

class Cube implements Figure3d
{
    public float $side;

    public function __construct($side)
    {
        $this->side = $side;
    }

    /**
     * @return float
     */
    public function getVolume(): float
    {
        return (pow($this->side, 3));
    }

    /**
     * @return float
     */
    public function getSurfaceSquare(): float
    {
        return (6 * pow($this->side, 2));
    }
}
