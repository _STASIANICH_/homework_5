<?php

namespace task_28;

interface Figure
{
    /**
     * @return mixed
     */
    public function getSquare(); // получить площадь  фигуры

    /**
     * @return mixed
     */
    public function getPerimeter(); // получить периметр фигуры
}
