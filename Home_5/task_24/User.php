<?php

declare(strict_types=1);

namespace task_24;

class User implements UserInterface
{
    private string $name;
    private int $age;

    /**
     * @param $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param $age
     */
    public function setAge($age): void
    {
        $this->age = $age;
    }

    /**
     * @return float
     */
    public function getAge(): float
    {
        return $this->age;
    }
}
