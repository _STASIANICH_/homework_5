<?php

namespace task_24;

interface UserInterface
{
    /**
     * @param $name
     * @return mixed
     */
    public function setName($name); // установить имя

    /**
     * @return mixed
     */
    public function getName(); // получить имя

    /**
     * @param $age
     * @return mixed
     */
    public function setAge($age); // установить возраст

    /**
     * @return mixed
     */
    public function getAge(); // получить возраст
}
