<?php

declare(strict_types=1);

namespace task_19;

class Driver extends Employee
{
    private float $experience;

    private string $category;

    /**
     * @return float
     */
    public function getExperience(): float
    {
        return $this->experience;
    }

    /**
     * @param float $experience
     */
    public function setExperience(float $experience): void
    {
        $this->experience = $experience;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }
}
