<?php

declare(strict_types=1);

namespace task_19;

class Employee extends User
{
    protected float $salary;

    /**
     * @param $salary
     */
    protected function setSalery($salary): void
    {
        $this->salary = $salary;
    }

    protected function getSalery(): float
    {
        return $this->salary;
    }
}
