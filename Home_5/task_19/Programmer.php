<?php

declare(strict_types=1);

namespace task_19;

class Programmer extends Employee
{
    private $langs = [];

    /**
     * @return array
     */
    public function getLang()
    {
        return $this->langs;
    }

    /**
     * @param string $newLang
     */
    public function setLang(string $newLang): void
    {
        $this->langs[] = $newLang;
    }
}
