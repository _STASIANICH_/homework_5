<?php

declare(strict_types=1);

namespace task_19;

class User
{
    private string $name;
    protected int $age;

    /**добавил новое свойство*/


    public function __set($name, $value): void
    {
        if (!isset($this->$name)) {
            $this->$name = $value;
        }
    }

    public function __get($name)
    {
        return $this->$name;
    }

    protected function setAgeV1($age): void
    {
        $this->age = $age;
    }

    public function setAgeV2($age): void
    {
        if($age >= 18) {
            $this->age = $age;
        }
    }
}
