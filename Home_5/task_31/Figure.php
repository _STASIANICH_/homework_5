<?php

declare(strict_types=1);

namespace task_31;

interface Figure
{
    /**
     * @return float
     */
    public function getArea(): float;

    /**
     * @return float
     */
    public function getPerimeter(): float;

    /**
     * @return float
     */
    public function getSum(): float;
}
