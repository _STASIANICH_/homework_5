<?php

declare(strict_types=1);

namespace task_31;

class FiguresCollection
{
    private $figureCollection = [];

    /**
     * @param $name
     * @return $this
     */
    public function addFigure($name)
    {
        $this->figureCollection[] = $name;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalSquare(): float
    {
        $sum = 0;
        foreach ($this->figureCollection as $figure){
            $sum += $figure->getArea();
        }
        return $sum;
    }
}
