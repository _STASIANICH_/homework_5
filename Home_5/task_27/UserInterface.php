<?php

namespace task_27;

interface UserInterface
{
    /**
     * @return mixed
     */
    public function getName(); // метод для получения имени юзера

    /**
     * @return mixed
     */
    public function getAge(); // метод для получения возроста юзера

    /**
     * @param string $name
     * @return mixed
     */
    public function setName(string $name); // метод для установки имени юзера

    /**
     * @param int $age
     * @return mixed
     */
    public function setAge(int $age); // метод для установки возроста юзера
}
