<?php

namespace task_27;

interface EmployeeInterface extends UserInterface
{
    /**
     * @return mixed
     */
    public function getSalary(); // метод для получения зарплаты юзера

    /**
     * @param float $salary
     * @return mixed
     */
    public function setSalary(float $salary); // метод для установки зарплаты юзера
}
