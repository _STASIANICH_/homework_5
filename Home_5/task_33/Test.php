<?php

declare(strict_types=1);

namespace task_33;

class Test
{
    use Trait1;
    use Trait2;
    use Trait3;

    /**
     * @return float
     */
    public static function getSum(): float
    {
        return (self::method1() + self::method2() + self::method3());
    }
}
