<?php

namespace task_33;

trait Trait2
{
    /**
     * @return float
     */
    private function method2(): float
    {
        return 2;
    }
}
