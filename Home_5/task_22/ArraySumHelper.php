<?php

declare(strict_types=1);

namespace task_22;

class ArraySumHelper
{
    /**
     * @param $arr
     * @param $power
     * @return mixed
     */
    public static function getPow($arr, $power)
    {
        foreach ($arr as $key => $element) {
            $arr[$key] = pow($arr[$key], $power);
        }
        return $arr;
    }

    /**
     * @param $arr
     * @return int|mixed
     */
    public static function getSum($arr): float
    {
        $sum = 0;
        foreach ($arr as $element) {
            $sum += $element;
        }
        return $sum;
    }
}
