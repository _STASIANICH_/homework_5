<?php

declare(strict_types=1);

namespace task_20;

class Teacher extends Student
{
    //private string $courseAdministratorPatronymic;

    public function validator($name, $surname, $patronymic)
    {
        if(parent::validator($name, $surname) && ctype_alpha($patronymic)){
            return true;
        }else{
            return false;
        }
    }

    public function setCourseAdministrator($name, $surname, $patronymic): void
    {
        if($this->validator($name, $surname, $patronymic)) {
            $this->courseAdministratorName = $name;
            $this->courseAdministratorSurname = $surname;
            $this->courseAdministratorPatronymic = $patronymic;
        }else{
            $this->courseAdministratorName = 'Error';
            $this->courseAdministratorSurname = 'Error';
        }
    }
}
