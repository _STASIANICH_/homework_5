<?php

declare(strict_types=1);

namespace task_20;

class Student
{
    private string $name;
    private int $course = 3;
    protected string $courseAdministratorName;
    protected string $courseAdministratorSurname;
    protected string $courseAdministratorPatronymic;


    public function transferToNextCourse(): void
    {
        if($this->isCourseCorrect()){
            $this->course++;
        }
    }

    private function isCourseCorrect(): bool
    {
        if (($this->course + 1) < 6) {
            return true;
        } else {
            return false;
        }
    }

    public function setCourseAdministrator($adminName, $adminSurname): void
    {
        if($this->validator($adminName, $adminSurname)) {
            $this->courseAdministratorName = $adminName;
            $this->courseAdministratorSurname = $adminSurname;
        }else{
            $this->courseAdministratorName = 'Error';
            $this->courseAdministratorSurname = 'Error';
        }
    }

    protected function validator($name, $surname)
    {
        if(ctype_alpha($name) && ctype_alpha($surname)){
            return true;
        }else{
            return false;
        }
    }

    public function getCourseAdministrator()
    {
        echo $this->courseAdministratorName;
        echo ' ' . $this->courseAdministratorPatronymic;
        echo ' ' . $this->courseAdministratorSurname . '<br>';
    }
}
